#include <avr/io.h>
#include <string.h>

#define FOSC 8000000
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

void setup_uart() {
	/* Baudrate einstellen */
	UBRR0H = MYUBRR >> 8;
	UBRR0L = MYUBRR;

	/* Empfänger und Sender einschalten */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
}

void uart_putchar(unsigned char c) {
	while (!(UCSR0A & (1 << UDRE0)))
		;
	UDR0 = c;
}

void uart_putstring(char *str) {
	for (unsigned char i = 0; i < 255; i++) {
		if (str[i] != 0)
			uart_putchar(str[i]);
		else
			break;
	}
}

char uart_getchar() {
	while (!(UCSR0A & (1 << RXC0)))
		;
	return (UDR0 );
}

void uart_readline(char *str) {
	char c;
	unsigned char index = 0;

	while (1) {
		c = uart_getchar();
		if (c != -1)
			if (c == 13) /* ASCII: NewLine */
			{
				/* Ende der Zeile erreicht, also String abschließen */
				str[index] = 0;
				return;
			} else {
				/* Normales Zeichen, anhängen an die Zeichenkette */
				str[index] = c;
				index++;
			}
	}
}

int main(void) {

	char kommando[255];
	char temp[10];
	unsigned char zahl = 0;

	/* Initializierung */
	setup_uart();

	/* Willkommensnachricht */
	uart_putstring("Serielle Kommunikation gestartet ...\r\n\r\n");

	while (1) {

		uart_readline(kommando);
		/* ==== BEFEHL: "version" */
		if (!strcmp(kommando, "version")) {
			uart_putstring("OK: Version 1.00\r\n");
		}

		/* ==== BEFEHL: "up" */
		else if (!strcmp(kommando, "up")) {
			if (zahl < 255) {
				zahl++;
				uart_putstring("OK: Zahl wurde erhoeht\r\n");
			} else
				uart_putstring("ERROR: Maximum erreicht (Zahl=255)\r\n");
		}

		/* ==== BEFEHL: "down" */
		else if (!strcmp(kommando, "down")) {
			if (zahl > 0) {
				zahl--;
				uart_putstring("OK: Zahl wurde verringert\r\n");
			} else
				uart_putstring("ERROR: Minimum erreicht (Zahl=0)\r\n");
		}

		/* ==== BEFEHL: "show" */
		else if (!strcmp(kommando, "show")) {
			itoa(zahl, temp, 10);
			uart_putstring("OK: Zahl = ");
			uart_putstring(temp);
			uart_putstring("\r\n");
		}
	}

}
